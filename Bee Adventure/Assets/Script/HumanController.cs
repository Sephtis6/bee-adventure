﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HumanController : Controller {

    public Transform tf;
    public float speed;
    public FollowPlayerCamera FPC;

    public float jumpForce;
    public float jumpsLeft;
    public float jumpsAvailable;
    public float timeTillJumpRespawn;
    public float timeTillJumpRespawnStatic;
    public Text JumpsLeftText;

    public float lives = 3;
    public Text LifeText;

    public float score;
    public Text ScoreText;

    [HideInInspector] public GameManager gm;
    public GameObject playerPrefab;
    public GameObject playerRespawnPoint;
    public GameObject player;


    public bool isGrounded;
    public float groundDistance = 0.1f;

    public int loseLevelToLoad;

    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        tf = GetComponent<Transform>();
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        lives = gm.playerLives;
        setPawn();
        gm.volumeAudio.clip = gm.audioClips[1];
        gm.volumeAudio.loop = true;
        gm.volumeAudio.Play();
        jumpsLeft = jumpsAvailable;
        score = gm.playerScore;
        setLifeText();
        SetJumpsLeft();
        SetScore(0);
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        timeTillJumpRespawn -= Time.deltaTime;
        if (timeTillJumpRespawn <= 0)
        {
            if (jumpsLeft < jumpsAvailable)
            {
                jumpsLeft += 1;
            }
            else if (jumpsLeft == jumpsAvailable)
            {
                jumpsLeft = jumpsAvailable;
            }
            SetJumpsLeft();
            timeTillJumpRespawn = timeTillJumpRespawnStatic;
        }
        Vector2 movementVector = Vector2.zero;
        //move straight
        if (Input.GetKey(KeyCode.W))
        {
            movementVector.y = 1;
        }
        //rotate left
        if (Input.GetKey(KeyCode.A))
        {
            movementVector.x = -1;
        }
        //rotate right
        if (Input.GetKey(KeyCode.D))
        {
            movementVector.x = 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            movementVector.y = -1;
        }

        pawn.Move(movementVector);
        // Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            pawn.Jump();
        }

        if (lives <= 0)
        {
            SceneManager.LoadScene(loseLevelToLoad);
        }
    }

    public void Spawn()
    {
        player = Instantiate(playerPrefab, playerRespawnPoint.transform.position, playerRespawnPoint.transform.rotation);
    }

    public void setLifeText()
    {
        LifeText.text = "Lives Left: " + lives.ToString();
        gm.playerLives = lives;
    }

    public void InstantiatePlayer()
    {
        Instantiate(playerPrefab, playerRespawnPoint.transform.position, Quaternion.identity);
    }

    public void setPawn()
    {
        gm.playerPawn = pawn;
    }

    public void SetScore(float flowerCost)
    {
        score = score + flowerCost;
        if (score == 3)
        {
            score = 0;
            lives += 1;
        }
        gm.playerScore = score;
        ScoreText.text = "Flowers Collected: " + score.ToString();
    }

    public void SetJumpsLeft()
    {
        JumpsLeftText.text = "Jumps Left: " + jumpsLeft.ToString();
    }
}
