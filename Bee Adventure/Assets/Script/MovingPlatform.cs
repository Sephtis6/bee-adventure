﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovingPlatform : MonoBehaviour
{
    float dirx, movespeed = 5;
    public bool moveRight;
    public float rightEdge;
    public float leftEdge;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position.x > rightEdge)
        {
            moveRight = false;
            Destroy(gameObject);
        }
        if (transform.position.x < leftEdge)
        {
            moveRight = true;
        }
        if (moveRight)
        {
            transform.position = new Vector2(transform.position.x + movespeed * Time.deltaTime, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x - movespeed * Time.deltaTime, transform.position.y);
        }
    }

    public void OnTriggerEnter2D(Collider2D otherCollider)
    {
        if (otherCollider.gameObject.tag == "Player")
        {
            HumanController CC = otherCollider.gameObject.GetComponent<HumanController>();
            Destroy(otherCollider);
            CC.lives -= 1;
            CC.setLifeText();
            CC.Spawn();
        }
    }

}
