﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : MonoBehaviour
{

    [HideInInspector] public HumanController CC;
    [HideInInspector] public Transform tf;

    //integers and floats named
    [HideInInspector] public Rigidbody2D rb;
    private SpriteRenderer sr;
    private Animator anim;


    public void Start()
    {
        CC = GameObject.Find("CharacterController").GetComponent<HumanController>();
        CC.pawn = GetComponent<Pawn>();
        tf = GetComponent<Transform>();
        CC.setPawn();

        // Get my components
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
    }

    public void Update()
    {
        //CheckForGrounded();
    }

    //checking for grounded using raycast
    public void CheckForGrounded()
    {
        RaycastHit2D hitInfo = Physics2D.Raycast(tf.position, Vector3.down, CC.groundDistance);
        if (hitInfo.collider == null)
        {
            CC.isGrounded = false;
        }
        else
        {
            CC.isGrounded = true;
            CC.jumpsLeft = CC.jumpsAvailable;
            CC.SetJumpsLeft();
            Debug.Log("Standing on " + hitInfo.collider.name);
        }
    }

    public void Move(Vector2 moveVector)
    {
        // Change X velocity based on speed and moveVector
        rb.velocity = new Vector2(moveVector.x * CC.speed, rb.velocity.y);

        // If velocity is <0, flip sprite. 
        if (rb.velocity.x < 0)
        {
            sr.flipX = true;
        }
        // If velocity is >0, flip sprite. 
        else if (rb.velocity.x > 0)
        {
            sr.flipX = false;
        }
    }

    //allows the character pawn to jump/double jump
    public void Jump()
    {
        if (CC.jumpsLeft == 0)
        {
            CC.jumpsLeft = 0;
        }
        if (CC.jumpsLeft > 0)
        {
            tf.position += tf.up * CC.jumpForce;
            CC.jumpsLeft -= 1;
        }
        CC.SetJumpsLeft();
    }

}